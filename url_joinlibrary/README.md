# url-join


本库由 [坚果](https://blog.csdn.net/qq_39132095),完成迁移。

## 一、下载安装

```
ohpm install @nutpi/url-join
```

OpenHarmony ohpm 环境配置等更多内容，请参考[如何安装 OpenHarmony ohpm 包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md)

## 二、使用

```js
import urlJoin from  "@nutpi/url-join";

@Entry
@Component
struct Index {
//https://github.com/jfromaniello/url-join

  build() {
    RelativeContainer() {
      Text(urlJoin('http://www.google.com', 'a', '/b/cd', '?foo=123', '&bar=456', '#heading-1').toString())
        .id('nutpi')
        .fontSize(50)
        .fontWeight(FontWeight.Bold)
        .alignRules({
          center: { anchor: '__container__', align: VerticalAlign.Center },
          middle: { anchor: '__container__', align: HorizontalAlign.Center }
        })
    }
    .height('100%')
    .width('100%')
  }
}
```



## 三、开源协议

本项目基于 [MIT](LICENSE) ，请自由地享受和参与开源。[感谢](https://github.com/jfromaniello/url-join)坚果派的小伙伴做出的努力。



## 四、运行环境

DevEco Studio NEXT Developer Beta1
Build Version: 5.0.3.401,

适用于API：12及以上，在真机Mate60测试ok。

